const _and = "and";
const _or = "or";
const excludeTypes = [undefined, null]

/**
 * The function expression generates condition javascript object from name, relation and value.
 * @param {*} name The name of DB field.
 * @param {*} relation The relation of prediction
 * @param {*} value The value of DB field
 */
exports.cond = (name, relation, value) => {
  return { name, relation, value };
}

/**
 * The function expression generates javascript object for 'and' expression.
 * @param {*} exp_1 The DB expression. It may be simple prediction ('status = 2') or complex group of predictions ('status = 2 or (rec_id > 10 and is_jur = 0)').
 * @param {*} exp_2 The DB expression. It may be simple prediction ('status = 2') or complex group of predictions ('status = 2 or (rec_id > 10 and is_jur = 0)').
 */
exports.and = (exp_1, exp_2, ...exp_n) => {
  return { and: [exp_1, exp_2].concat(exp_n).filter(exp => !excludeTypes.includes(exp)) };
}

/**
 * The function expression generates javascript object for 'or' expression.
 * @param {*} exp_1 The DB expression. It may be simple prediction ('status = 2') or complex group of predictions ('status = 2 or (rec_id > 10 and is_jur = 0)').
 * @param {*} exp_2 The DB expression. It may be simple prediction ('status = 2') or complex group of predictions ('status = 2 or (rec_id > 10 and is_jur = 0)').
 */
exports.or = (exp_1, exp_2, ...exp_n) => {
  return { or: [exp_1, exp_2].concat(exp_n).filter(exp => !excludeTypes.includes(exp)) };
}

/**
 * The function expression generates javascript object for where clause. If parameter 'exp' is single condition, The function expression generates appopriate javascript object.
 * @param {*} exp The DB expression, that may be single condition or group of conditions
 * @param {*} operation The operation type for single condition. Default value is 'and'.
 */
exports.where = (exp, operation = _and) => {
  if (excludeTypes.includes(exp)) {
    return {where: {}};
  }
  let whereClause = { where: exp };
  if (!(_and in exp) && !(_or in exp)) { // if 'exp' is single condition
    Object.assign(whereClause, {where: {[operation]: [exp]}})
  }
  return whereClause;
}

/**
 * The function expression generates select statement.
 * @param {*} fields The fields name that must be selected. If it is empty, The function expression generates select statement for all fields.
 */
exports.select = (...fields) => {
  return { select: fields.length === 0 ? ["*"] : fields };
}

/**
 * The function expression generates javascript object for field and its order value.
 *  @param {*} field The name of DB field.
 *  @param {*} order The value of order: "desc", "asc"
 */
exports.field_order = (field, order) => {
  return {[field]: order};
}

/**
 * The function expression generates order_by clause from given fields order.
 * @param {*} field_orders javascript object Array of fields order.
 */
exports.order_by = (...field_orders) => {
  return { order_by: field_orders };
}

/**
 * The function expression generates full javascript object for DB options: select, where, order_by etc.
 * @param {*} options javascript object Array of DB statements or clauses.
 * Usage: merge(select('rec_id', 'email'), where(cond('status', '=', 2)), order_by(field_order('email', 'desc')))
 */
exports.merge = (...options) => {
  let result = {};
  options.forEach(opt => Object.assign(result, opt));
  return result;
}
